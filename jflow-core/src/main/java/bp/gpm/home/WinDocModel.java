package bp.gpm.home;

public class WinDocModel {
	/** 
	 Html
	*/
	public static final String Html = "Html";
	/** 
	 文本变量
	*/
	public static final String HtmlVar = "HtmlVar";
	/** 
	 系统内置
	*/
	public static final String System = "System";
	/** 
	 SQL列表
	*/
	public static final String Table = "Table";
	/** 
	 折线图
	*/
	public static final String ChartZZT = "ChartZZT";
	/** 
	 柱状图
	*/
	public static final String ChartLine = "ChartLine";
	/** 
	 饼图
	*/
	public static final String ChartPie = "ChartPie";
	/** 
	 扇形图
	*/
	public static final String ChartRate = "ChartRate";
	/** 
	 环形图
	*/
	public static final String ChartRing = "ChartRing";
	/// <summary>
    /// 标签页
    /// </summary>
    public static final String Tab = "Tab";

}
