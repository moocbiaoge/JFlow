package bp.wf.port;
import bp.en.EntityNoNameAttr;
/** 
 工作人员属性
*/
public class EmpAttr extends EntityNoNameAttr
{

		///基本属性
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
	///// <summary>
	///// 单位
	///// </summary>
	//public const string FK_Unit = "FK_Unit";
	/** 
	 密码
	*/
	public static final String Pass = "Pass";
	/** 
	 SID
	*/
	public static final String SID = "SID";
	/** 
	 手机号码
	*/
	public static final String Tel = "Tel";

		///
}