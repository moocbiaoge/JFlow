package bp.ccbill;

import bp.ccbill.template.*;

/** 
 表单活动类型
*/
public class FrmActionType
{
	/// <summary>
	/// 创建
	/// </summary>
	public static final String Create = "Create";
	/// <summary>
	///保存
	/// </summary>
	public static final String Save = "Save";
	/// <summary>
	/// 提交
	/// </summary>
	public static final String Submit = "Submit";
	/// <summary>
	/// 评论
	/// </summary>
	public static final String BBS = "BBS";
	/// <summary>
	/// 打开
	/// </summary>
	public static final String View = "View";
	/// <summary>
	/// 回滚数据
	/// </summary>
	public static final String DataVerReback = "DataVerReback";
	/// <summary>
	/// 发起流程
	/// </summary>
	public static final String StartFlow = "StartFlow";
	/// <summary>
	/// 发起注册流程
	/// </summary>
	public final  static String StartRegFlow = "StartRegFlow";
	/// <summary>
	/// 其他
	/// </summary>
	public static final String Etc = "Etc";
}